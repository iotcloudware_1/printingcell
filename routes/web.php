<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function (){

    Route::get('add-costing', ['uses'=>'CustomerController@dummyIndex', 'as'=>'users.index']); // =========Dummy
	
    Route::get('/','ProductController@dashboard');
    Route::post('/filterizedproduct', 'ProductCostingController@filterizedProduct')->name('filterizedproduct');
	// Route::get('/', function(){
	// 	return view('welcome');
	// });

    // Route::get('/', 'CustomerController@test');
    // Route::get('add-costing', 'ProductCostingController@index');

    Route::resource('customers','CustomerController');
    Route::get('customer/{id}/delete','CustomerController@remove');
    Route::post('/customer/update-status', 'CustomerController@statusUpdated');

    Route::resource('vendors','VendorController');
    Route::get('vendor/{id}/delete','VendorController@remove');
    Route::get('/product-data/{id}','ProductCostingController@getDetails');
    Route::post('/vendor/update-status', 'VendorController@statusUpdated');

    Route::post('/store-costing', 'ProductCostingController@storeCosting')->name('store-costing');

    Route::resource('boards','BoardController');
    Route::resource('grammage','GrammageController');
    Route::get('/board/{id}/delete', 'BoardController@remove');
    Route::get('/vendor/{id}/delete', 'VendorController@remove');
    Route::get('/customer/{id}/delete', 'CustomerController@remove');
    Route::get('/grammage/{id}/delete', 'GrammageController@remove');

    Route::resource('products','ProductController')->middleware('optimizeImages');
    Route::get('product-costing/{id}/history', 'ProductCostingController@updatedCosting');



    // Route::get('addcosting','ProductController@addCosting'); 
    // Route::get('store-costing','ProductController@storeCosting')->name('storeCosting'); 

    Route::get('/customer/{id}/get-product', 'ProductController@getProduct');
    Route::get('/product/{id}/details', 'ProductCostingController@getDetails');
    Route::get('/product-history/{id}/details', 'ProductCostingController@getUpdatedDetails');
    Route::get('/product/{id}/delete', 'ProductController@remove');

    Route::get('/product/{id}/edit', 'ProductCostingController@editCosting');
    //Profile
    Route::group(['prefix' => 'profile'], function (){
        Route::get('',function(){
            return view('profile.index');
        });
        Route::get('change-password',function(){
            return view('profile.change-password');
        });
        Route::post('change_password','Auth\ForgotPasswordController@changePassword')->name('change_password');
    });
	
});

