<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('getauthuser','VendorController@getAuthUser');
Route::get('getvendors','VendorController@getVendors');
Route::get('getboards','BoardController@getBoards');
Route::get('getboarddetails','BoardController@getBoardDetails');
Route::get('getboardgrammage','BoardController@getBoardGrammage');
Route::get('getgrammageprice','BoardController@getGrammagePrice');
Route::get('getidgramage','BoardController@getIdGramage');
Route::post('submitcosting','ProductCostingController@submitCosting');
Route::post('updatecosting','ProductCostingController@updateCosting');