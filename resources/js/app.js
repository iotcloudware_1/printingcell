/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


Vue.component('costing-export-component', require('./components/CostingExport.vue').default);



Vue.component('costing-export-component-view', require('./components/CostingExportView.vue').default);

Vue.component('costing-local-component', require('./components/CostingLocal.vue').default);
Vue.component('costing-local-component-view', require('./components/CostingLocalView.vue').default);

Vue.component('proofing-export-component', require('./components/ProofingExport.vue').default);
Vue.component('proofing-export-component-view', require('./components/ProofingExportView.vue').default);

Vue.component('proofing-local-component', require('./components/ProofingLocal.vue').default);
Vue.component('proofing-local-component-view', require('./components/ProofingLocalView.vue').default);

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";
import VueTheMask from 'vue-the-mask';
import moment from 'moment';
import { MaskedTextBoxPlugin } from "@syncfusion/ej2-vue-inputs";


Vue.prototype.moment = moment
Vue.use(Vuetify); 
Vue.component("v-select", vSelect);
Vue.use(VueTheMask);
Vue.use(MaskedTextBoxPlugin);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

  const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
   
});
