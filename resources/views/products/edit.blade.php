@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Edit Product</h3>
            </div>
            <form method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
               {{csrf_field()}}
               {{method_field('PUT')}}
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Customer Name</label>
                        <select name="customer_id" class="form-control" required>
                        <option disabled="disabled" selected="selected">{{$product->customer_id}}</option>
                          @foreach($customer as $c)
                              <option value="{{$c['id']}}" >
                                  {{$c['name']}}
                              </option>
                          @endforeach
                      </select>
                     </div>
                  </div>
                  <!-- ad_link  --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Product Name</label>
                        <input name="name" value="{{$product->name}}" placeholder="company name" class="form-control" type="text" required>
                     </div>
                  </div>
                 
               </div>
               <div class="row">
                  <!-- category_type --verified -->
                   <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Description</label>
                     
                      <textarea name="description" class="form-control" placeholder="Description">{{ ucfirst($product->description) }}</textarea>  
                     </div>
                  </div> 
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Product Image</label>
                        <input name="image" placeholder="base image" class="form-control" type="file">
                     </div>
                      <div class="row">
                         <div class="col-md-3"><img class="thumbnail" height="100" src="{{ asset($product->image) }}" ></div>
                     </div>
                  </div>   
                   
               </div>            
                <input type="hidden" name="product_id" value="{{$product->id}}">
               <button class="btn btn-success">Update</button>
            </form>
         </div>
         <!-- Form -->
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop