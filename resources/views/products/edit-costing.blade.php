@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
 	@if($input['is_proofing'] == 0 && $input['is_local'] == 1 )
		<span style="font-size: 30px">Update Export Costing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>
	@elseif($input['is_proofing'] == 0 && $input['is_local'] == 0)	
		<span style="font-size: 30px">Update Local Costing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>
	@elseif($input['is_proofing'] == 1 && $input['is_local'] == 0)	
		<span style="font-size: 30px">Update Local Proofing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>
	@elseif($input['is_proofing'] == 1 && $input['is_local'] == 1)	
		<span style="font-size: 30px">Update Export Proofing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>		
	@endif
@stop

@section('content')
	@if($input['is_proofing'] == 0 && $input['is_local'] == 1 )
		<costing-export-component-view :costing="{{ json_encode($input) }}" :temp="{{ json_encode($temp) }}"></costing-export-component-view>
	@elseif($input['is_proofing'] == 0 && $input['is_local'] == 0)	
		<costing-local-component-view :costing="{{ json_encode($input) }}" :temp="{{ json_encode($temp) }}"></costing-local-component-view>
	@elseif($input['is_proofing'] == 1 && $input['is_local'] == 0)	
		<proofing-local-component-view :costing="{{ json_encode($input) }}" :temp="{{ json_encode($temp) }}"></proofing-local-component-view>	
	@elseif($input['is_proofing'] == 1 && $input['is_local'] == 1)	
		<proofing-export-component-view :costing="{{ json_encode($input) }}" :temp="{{ json_encode($temp) }}"></proofing-export-component-view>		
	@endif

@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/app.css"> -->
@stop

@section('js')

<!-- <script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('js/customer-based-product.js')}}"></script> -->

@stop