@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
 	@if($input['type'] == 0 && $input['quality'] == 1 )
		<span style="font-size: 30px">Export Costing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>
	@elseif($input['type'] == 0 && $input['quality'] == 0)	
		<span style="font-size: 30px">Local Costing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>
	@elseif($input['type'] == 1 && $input['quality'] == 0)	
		<span style="font-size: 30px">Local Proofing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>
	@elseif($input['type'] == 1 && $input['quality'] == 1)	
		<span style="font-size: 30px">Export Proofing Of </span><span style="font-size: 30px;color: #800080;font-weight: bold">{{App\Helpers\Helper::productIdToName($input['product_id'])}} </span>		
	@endif
@stop

@section('content')
	@if($input['type'] == 0 && $input['quality'] == 1 )
		<costing-export-component :costing="{{ json_encode($input) }}"></costing-export-component>
	@elseif($input['type'] == 0 && $input['quality'] == 0)	
		<costing-local-component :costing="{{ json_encode($input) }}"></costing-local-component>
	@elseif($input['type'] == 1 && $input['quality'] == 0)	
		<proofing-local-component :costing="{{ json_encode($input) }}"></proofing-local-component>
	@elseif($input['type'] == 1 && $input['quality'] == 1)	
		<proofing-export-component :costing="{{ json_encode($input) }}"></proofing-export-component>		
	@endif

@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/app.css"> -->
@stop

@section('js')

<!-- <script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('js/customer-based-product.js')}}"></script> -->

@stop