  @extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')
  <!-- Content Header (Page header) -->
    <section class="content-header mt-n4">
          
      <div class="container-fluid">
        <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
            <span style="font-size: 0px; border-radius:0px;" class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
            <div class="w3-modal-content w3-animate-zoom">
              <img id="img01" style="width:100%">
            </div>
          </div>
        <div class="row">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active"><a href="/add-costing">Add Costing</a></li>
              <li class="breadcrumb-item active">Costing's Details</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="row">
      <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Product Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>Name</th>
                      <th>Company</th>
                      <th>Description</th>
                      <th>Created At</th>
                    </tr>


                    </thead>
                    <tbody>
                    <tr>
                      <td style="color: red; font-size: 20px">{{$data->name}}</td>
                      <td style="color: red; font-size: 20px" >{{$company}}</td>
                      <td style="color: red; font-size: 20px">{{$data->description}}</td>
                      <td style="color: red; font-size: 20px">{{$data->created_at}}</td>

                    </tr>
                    <tr>
                      <th>Board</th>
                      <th>Type</th>
                      <th>Quality</th>
                      <!-- <th>Created By</th> -->
                    </tr>
                    <tr>
                      <td style="color: red; font-size: 20px">{{App\Helpers\Helper::boardToName($temp->is_regular_board)}}</td>
                      <td style="color: red; font-size: 20px">{{App\Helpers\Helper::proofingIdToName($temp->is_proofing)}}</td>
                      <td style="color: red; font-size: 20px">{{App\Helpers\Helper::qualityIdToName($temp->is_local)}}</td>
 <!--                      <td style="color: red; font-size: 20px">{{App\Helpers\Helper::userIdToName($temp->created_by)}}</td> -->

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
      <div class="col-md-4">                  
        <div class="card">
          <div class="card-header">
            <h3 style="font-weight: bold; font-size: 30px" class="card-title">Product Image</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <center>
                  <img  style="width:100%;cursor:pointer" onclick="onClick(this)" class="w3-hover-opacity" src="{{asset($data->image)}}" alt="Lights"> 
                
                </center>                       
              </div>
          
            </div>
          </div>

        </div>                  
      </div>

    </div>

    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Board Details</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr style="text-align: center;">
<!--                       <th>Created By</th>
                      <th>Type</th>
                      <th>Board</th>
                      <th>Quality</th> -->
                      <th>Board Width</th>
                      <th>Board Length</th>
                      
                      <th>Board Gramage</th>
                      <th>Board Price</th>
                      <th>Board Box Transport</th>
                      <th># Of Board Packets</th>
                      <th># Of Printed Boxes</th>
                      <th>Board Cost Per Box</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">  
<!--                       <td>{{App\Helpers\Helper::userIdToName($temp->created_by)}}</td>
                      <td>{{App\Helpers\Helper::proofingIdToName($temp->is_proofing)}}</td>
                      <td>{{App\Helpers\Helper::boardToName($temp->is_regular_board)}}</td>
                      <td>{{App\Helpers\Helper::qualityIdToName($temp->is_local)}}</td> -->
                      
                      <td>{{$temp->board_width}}</td>
                      <td>{{$temp->board_length}}</td>
                      <td>{{$temp->board_gramage}}</td>
                      <td>{{$temp->board_price}}</td>
                      <td>{{$temp->board_box_transport}}</td>
                      <td>{{$temp->no_of_board_packets}}</td>
                      <td>{{$temp->no_of_printed_boxes}}</td>
                      <td style="font-size: 30px;color: red">{{$temp->board_cost_per_box}}</td> 

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
    </div>
    @if($temp->total_printing_cost_per_box) 
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <!-- /.card-header -->
             <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Printing Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr style="text-align: center;">
                      
                      <th># Of Colors Width</th>
                      <th>Printing Cost</th>
                      <th># Of Ups</th>
                      <th>Total Printing Cost per Box</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">
                      
                      <td>{{$temp->no_of_colors}}</td>
                      <td>{{$temp->printing_cost}}</td>
                      <td>{{$temp->no_of_ups}}</td>
                      <td style="color: red;font-size: 30px">{{$temp->total_printing_cost_per_box}}</td>

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
    </div>
    @endif
    @if($temp->pasting_labor_cost_per_box && $temp->packing_rate_per_box ) 
    <div class="row">
      <div class="col-md-8">
          <div class="card">
            <!-- /.card-header -->
             <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Packing Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr style="text-align: center;">
                      
                      <th>Carton Price</th>
                      <th># Of Boxes in Carton</th>
                      <th>Packing Rate Per Box</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">
                      
                      <td>{{$temp->carton_price}}</td>
                      <td>{{$temp->no_of_boxed_in_carton}}</td>
                      <td style="color: red;font-size: 30px">{{$temp->packing_rate_per_box}}</td>

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
      <div class="col-md-4">
          <div class="card">
            <!-- /.card-header -->
             <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Pasting Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr style="text-align: center;">
                      
                      <th>Pasting Labor Cost Per Box</th>
                
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">
                      <td style="color: red;font-size: 30px">{{$temp->pasting_labor_cost_per_box}}</td>

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
    </div>
    @elseif(!$temp->pasting_labor_cost_per_box && $temp->packing_rate_per_box )
    <div class="row"> 
      <div class="col-md-12">
          <div class="card">
            <!-- /.card-header -->
             <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Packing Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr style="text-align: center;">
                      
                      <th>Carton Price</th>
                      <th># Of Boxes in Carton</th>
                      <th>Packing Rate Per Box</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">
                      
                      <td>{{$temp->carton_price}}</td>
                      <td>{{$temp->no_of_boxed_in_carton}}</td>
                      <td style="color: red;font-size: 30px">{{$temp->packing_rate_per_box}}</td>

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
    </div>
    @elseif($temp->pasting_labor_cost_per_box && !$temp->packing_rate_per_box )
    <div class="row"> 
      <div class="col-md-12">
          <div class="card">
            <!-- /.card-header -->
             <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Pasting Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr style="text-align: center;">
                      
                      <th>Pasting Labor Cost Per Box</th>
                
                    </tr>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">
                      <td style="color: red;font-size: 30px">{{$temp->pasting_labor_cost_per_box}}</td>

                    </tr>
                    </tbody>
                  </table>                  
                </div>
            
              </div>
            </div>
          </div>        
      </div>
    </div>    
    @endif
  @if($temp->lamination_cost_per_box)  
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Lamination Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align: center;">
                        <th>Lamination Width</th>
                        <th>Lamination Length</th>
                        <th>Lamination Cost Per Square Inch</th>
                        <th>Lamination Cost per Box</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style="text-align: center;">
                        <td>{{$temp->lamination_width}}</td>
                        <td>{{$temp->lamination_length}}</td>
                        <td>{{$temp->lamination_cost_per_square_inch}}</td>
                        <td style="color: red;font-size: 30px">{{$temp->lamination_cost_per_box}}</td>
                      </tr>
                    </tbody>
                  </table>                  
                </div>
              </div>
            </div>
          </div>        
      </div>
    </div>
    @endif
    @if($temp->corrugation_rate_per_box) 
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Corrugation Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align: center;">
                        <th>Width</th>
                        <th>Cutting Size</th>
                        <th>Cost Per Square Inch</th>
                        <th>Transport</th>
                        <th>Glue Rate</th>
                        <th>Corrugation Cost</th>
                        <th>Corrugation Glue Cost</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style="text-align: center;">
                        <td>{{$temp->corrugation_width}}</td>
                        <td>{{$temp->corrugation_cutting_size}}</td>
                        <td>{{$temp->corrugation_per_square_inch_cost}}</td>
                        <td>{{$temp->corrugation_transport}}</td>
                        <td>{{$temp->glue_rate}}</td>
                        <td style="color: red;font-size: 30px">{{$temp->corrugation_rate_per_box}}</td>
                        <td style="color: red;font-size: 30px">{{$temp->corrugation_glue_rate_per_box}}</td>
                      </tr>
                    </tbody>
                  </table>                  
                </div>
              </div>
            </div>
          </div>        
      </div>
    </div>
    @endif
    @if($temp->plate_and_dye_rate_per_box) 
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Play & Dye Cost Details</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align: center;">
                        <th># Of Colours</th>
                        <th>Per Plate Cost</th>
                        <th>Plate Cost</th>
                        <th>Dye Cost</th>
                        <th>Profit Percentage</th>
                        <th>Plate & Dye Cost</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr style="text-align: center;">
                        <td>{{$temp->colors_plate_cost}}</td>
                        <td>{{$temp->per_plate_cost}}</td>
                        <td>{{$temp->plate_cost}}</td>
                        <td>{{$temp->dye_cost}}</td>
                        <td>{{$temp->profit_percentage_of_dye_plates}} %</td>
                        <td style="color: red;font-size: 30px">{{$temp->plate_and_dye_rate_per_box}}</td>
                      </tr>
                    </tbody>
                  </table>                  
                </div>
              </div>
            </div>
          </div>        
      </div>
    </div>
    @endif
    <div class="row">
      <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 style="font-weight: bold; font-size: 30px" class="card-title">Calculated Cost</h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr style="text-align: center;">
                        <th>Profit Percentage</th>
                        <th>Profit Percentage Price</th>
                        <th>Total Cost Per Box</th>
                        <th>Final Cost</th>
                        @if($temp->discounted_price)
                        <th>Discounted Price</th>
                        @endif
                        <th>Costing Date And Time</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                      <tr style="text-align: center;">
                        <td >{{$temp->profit_percentage_of_total_price}} %</td>

                        <td style="color: red;font-size: 30px">{{ substr($temp->profit_percentage_calculation, 0,  4) }}</td>
                        <td style="color: red;font-size: 30px">{{$temp->total_cost_per_box}}</td>
                        <td style="color: red;font-size: 30px">{{$temp->total_price_per_box_comments}}</td>
                        @if($temp->discounted_price)
                        <td style="color: red;font-size: 30px">{{$temp->discounted_price}}</td>
                        @endif
                        <td>{{$temp->costing_date_and_time}}</td>
                      </tr>
                    </tbody>
                  </table>                  
                </div>
              </div>
            </div>
          </div>        
      </div>
    
    </div>



@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

@stop

@section('js')
<script>
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
}
</script>
@stop