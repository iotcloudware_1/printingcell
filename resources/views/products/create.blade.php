@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Create Product</h3>
            </div>
            <form method="post" action="{{ route('products.store') }}" enctype="multipart/form-data">
               @csrf
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Company Name</label>
                        <select name="customer_id" class="form-control" required>
                        <option disabled="disabled" selected="selected">Select Company</option>
                          @foreach($customer as $c)
                              <option value="{{$c['id']}}" >
                                  {{$c['company_name']}}
                              </option>
                          @endforeach
                      </select>
                     </div>
                  </div>
                  <!-- ad_link  --verified -->
                  
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label> Product Name</label>
                        <input name="name" placeholder="product name" class="form-control" type="text" required>
                     </div>
                  </div>
                 
               </div>
               <div class="row">
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Product Image</label>
                        <input name="image[]" placeholder="base image" class="form-control" type="file" multiple required>
                     </div>
                     
                  </div>   
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Description</label>
                     
                           <textarea name="description" class="form-control" placeholder="Description"></textarea>
                     </div>
                  </div>

                 
               </div>               

               <button class="btn btn-success">Create</button>
            </form>
         </div>
         <!-- Form -->
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop