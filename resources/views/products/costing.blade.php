@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
<div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
           <!--  <div class="heading-panel">
               <h3 class="main-title text-left">Create Product</h3>
            </div> -->
            <form id="my-form" method="post" action="{{route('store-costing')}}" enctype="multipart/form-data">
               @csrf
               <div class="row">

                @if(Auth::check())
                  <input type="hidden" name="created_by" value="{{Auth::user()->id}}">
                @endif
                  <!-- category_type --verified -->
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Company Name</label>
                        <select class="customer-select form-control" required name="customer_id">
                        <option disabled="disabled" selected="selected">Select Company </option>
                           @foreach(Helper::customers() as $c)
                           <option value="{{$c['id']}}" >
                              {{$c['company_name']}}
                           </option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Product Name</label>
                        <div id="product_parent">
                            <select class="product-select form-control" required name="product_id">
                              <option disabled="disabled" selected="selected">Select Product </option>
                              @foreach(Helper::products() as $p)
                                  <option value="{{$p['id']}}" >
                                      {{$p['name_ar']}}
                                  </option>
                              @endforeach
                            </select>
                            
                          </div> 
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Type</label>
                        <select id="type" name="type" class="form-control" required>
                        <option disabled="disabled" selected="selected">Select Type</option>
                          <option value="0">Costing</option>
                          <option value="1">Proofing</option>
                          
                      </select>
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Quality </label>
                        <select id="quality" name="quality" class="form-control" required>
                          <option disabled="disabled" selected="selected">Select quality</option>
                          <option value="0">Local</option>
                          <option value="1">Export</option>
                          
                      </select>
                     </div>
                  </div>
                  
                 
               </div>             

               <button class="btn btn-success"onclick="return clicked();">Add Costing</button>
            </form>
         </div>
         <!-- Form -->
  </div>
  {{$dataTable->table()}}

    <style>
        #append-user-modal{ z-index: 9999 !important; }
    </style>
    <div class="modal fade" id="append-user-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">User Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <div class="col-md-12" id="append-user-data" ></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/app.css"> -->
@stop

@section('js')
{{$dataTable->scripts()}}
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{ asset('js/load-admin-product-data.js') }}"></script>
<script src="{{ asset('js/customer-based-product.js') }}"></script>
<script type="text/javascript">
function clicked() {
      let price       = $('#type').val();
      let product       = $('#product_id').val();
      let quality       = $('#quality').val();

      if(price == 0)
      {
        price='Costing';
      }
      else if(price == 1)
      {
        price='Proofing';
      }
      if(quality == 0)
      {
        quality='Local';
      }
      else if(quality == 1)
      {
        quality='Export';
      }


    return confirm('Are you sure you want to add '+ price+' of '+ quality+ ' quality?');
}
</script>
<!-- <script type="text/javascript">
  // function add()
  // {
    //get the closable setting value.
    // var closable = alertify.alert().setting('closable');
    //grab the dialog instance using its parameter-less constructor then set multiple settings at once.
    // alertify.alert()
    //   .setting({
    //     'label':'Agree',
    //     'message': 'This dialog is : ' + (closable ? ' ' : ' not ') + 'closable.' ,
    //     'onok': function(){ alertify.success('Great');}
    //   }).show();;

  // }
$(document).ready( function() { // Wait until document is fully parsed
  $("#my-form").on('submit', function(e){

     e.preventDefault();

  });
})
</script> -->
@stop