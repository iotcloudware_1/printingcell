@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
  <h2>View</h2>
<proofing-export-component-view></proofing-export-component-view>

@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/app.css"> -->
@stop

@section('js')
{{$dataTable->scripts()}}
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{ asset('js/load-admin-product-data.js') }}"></script>
<script src="{{ asset('js/customer-based-product.js') }}"></script>


@stop