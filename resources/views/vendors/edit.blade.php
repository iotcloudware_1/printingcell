@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Edit Vendor</h3>
            </div>
            <form method="POST" action="{{ route('vendors.update', $vendor->id) }}" enctype="multipart/form-data">
               {{csrf_field()}}
               {{method_field('PUT')}}
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>vendor Name</label>
                        <input name="name" placeholder="vendor name" value="{{$vendor->name}}" class="form-control" type="text" required>
                     </div>
                  </div>
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Mobile</label>
                        <input name="mobile" value="{{$vendor->mobile}}" placeholder="phone no" class="form-control" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "11" required>
                     </div>
                  </div>                 
               </div>
               <div class="row">
                  <!-- category_type --verified -->

                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Email</label>
                        <input name="email" value="{{$vendor->email}}" placeholder="email" class="form-control" type="email" required>
                     </div>
                  </div>
                   <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Description</label>
                     
                      <textarea name="description" class="form-control" placeholder="Description">{{ ucfirst($vendor->description) }}</textarea>  
                     </div>
                  </div> 
                   
               </div>            
                <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
               <button class="btn btn-success">Update</button>
            </form>
         </div>
         <!-- Form -->
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop