@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Create Vendor</h3>
            </div>
            <form method="post" action="{{ route('vendors.store') }}" enctype="multipart/form-data">
               @csrf
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Vendor Name</label>
                        <input name="name" placeholder="vendor name" class="form-control" type="text" required>
                     </div>
                  </div>
                  <!-- ad_link  --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Mobile</label>
                        <input name="mobile" placeholder="Enter Phone No" class="form-control"
                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"maxlength = "11" type="number" required>
                     </div>
                  </div>                 
               </div>
               <div class="row">
                  <!-- category_type --verified -->

                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Email</label>
                        <input name="email" placeholder="email" class="form-control" type="email" required>
                     </div>
                  </div>

                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Description</label>
                     
                           <textarea name="description" class="form-control" placeholder="Description"></textarea>
                     </div>
                  </div>                 
               </div>              

               <button class="btn btn-success">Create</button>
            </form>
         </div>
         <!-- Form -->
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')

@stop