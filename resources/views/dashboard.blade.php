@extends('adminlte::page')

@section('title', 'Dashboard')
@section('content_header')
    <h1>Products</h1>
@stop

@section('content')
<div class="container">

	<h3>Apply Filter</h3>
	@if($filtered == 'filtered')
		@if($new)
		<span style="font-size: 22px">Product  &nbsp;&nbsp; </span>
			@foreach($new as $n)
				<span style="font-size: 20px;color: blue">{{App\Helpers\Helper::productIdToName($n)}} &nbsp;&nbsp;</span>

			@endforeach
		@endif
		<br />
		@if($type)
			@if($type == 2)			
			<span style="font-size: 22px">Type  &nbsp;&nbsp;</span><span style="font-size: 20px;color: red">{{App\Helpers\Helper::proofingIdToName(0)}} &nbsp;&nbsp;</span>
			@else
			<span style="font-size: 22px">Type  &nbsp;&nbsp;</span><span style="font-size: 20px;color: red">{{App\Helpers\Helper::proofingIdToName($type)}} &nbsp;&nbsp;</span>
			@endif
		@endif
	@endif
	
	<form action="{{route('filterizedproduct')}}" method="POST">
		@csrf
		<div class="row">
			<label style="margin-top: 20px">Select Product</label>
			<div class="col-md-5">
				<select class="js-example-basic-multiple js-states form-control" name="states[]" multiple="multiple">
					@foreach($product as $p)

				  	<option value="{{$p['id']}}">{{$p['name']}}</option>
				 	@endforeach 
				</select>						
			</div>
			<label style="margin-top: 20px">Select Type</label>
			<div class="col-md-4">
				<select class="js-example-basic-single js-states form-control" name="type">
				  	<option disabled="disabled" selected="selected">Select Type</option>
				  	<option value="2">Costing</option> 
				  	<option value="1">Proofing</option> 
				</select>						
			</div>
			<button style="color: white" type="submit" class="btn btn-success">Apply Filter</button>	&nbsp;	
				
		</div>
	</form>	
	<a href="/"><button style="color: white;margin-top: -62px; margin-left: 120px" type="submit" class="btn btn-success">Remove Filter</button></a>	
	<div class="row">
		<div class="col-md-12">
	  	    <div class="row" >
	        @foreach(range(1, 1) as $key => $value)
		        @if(!($key % 4) and $key > 0) 
		        @endif
		    </div>
		   
		    <br>
		    <div class="row">
		    @foreach($tailor as $t)           
			    <div class="col-md-3 sm-box">
			        <div>
				        <center>
				          <img style="cursor:pointer;border: 0.5px solid grey;border-radius: 15px" onclick="onClick(this)" class="responsive w3-hover-opacity" src="{{App\Helpers\Helper::productIdToImage($t['product_id'])}}"  alt="Lights"><h2 style="font-weight: bold; font-size:20px ">{{App\Helpers\Helper::productIdToName($t['product_id'])}}</h2>
				          
				          <span class="badge bg-success">{{App\Helpers\Helper::proofingIdToName($t['is_proofing'])}}</span>
				          <span class="badge bg-info">{{App\Helpers\Helper::qualityIdToName($t['is_local'])}}</span>
				          <span class="badge bg-info">{{App\Helpers\Helper::boardToName($t['is_regular_board'])}}</span>
				          <a href="/product/{{$t['product_costing_id']}}/details"><button class="badge bg-success"> View</button></a>
				          @if($t['discounted_price'])
				          <h4 style="font-size: 20px">Final Discounted Price</h4>
				          @else
				          <h4 style="font-size: 20px">Final Cost</h4>
				          @endif
				          <div style="background-color: lightblue;border-radius: 15px">
				          	@if($t['discounted_price'])
				          	<span style="font-size: 30px">{{$t['discounted_price']}}</span>&nbsp;&nbsp;<span class="c">{{$t['total_price_per_box_comments']}}	</span>
				          	@else
				          	<h2>{{$t['total_price_per_box_comments']}}</h2>
				          	@endif
				          </div>
				        </center>
			        </div>
			     <br />
			    </div>

		    @endforeach

		    @endforeach

		    </div>
		</div>
	</div>
	<div id="modal01" class="w3-modal" onclick="this.style.display='none'">
	    <span style="font-size: 0px; border-radius:0px;" class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
	    <div class="w3-modal-content w3-animate-zoom">
	      <img  id="img01" style="width:100%">
	    </div>
	</div>	
</div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style type="text/css">
    	.c {
		   -webkit-text-decoration-line: line-through; /* Safari */
		   text-decoration-line: line-through; 
		}
		 .responsive {
		  max-width: 100%; display:block; height: 100px;
		}
		.select2-container--default .select2-selection--multiple .select2-selection__choice__display
		{
			color: red;
		}
    </style>

@stop

@section('js')

<script>
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
}

$(document).ready(function() {
    $('.js-example-basic-multiple').select2();
    console.log("agyyy")
    // let temp=  $('.js-example-basic-multiple').val();
    
});
</script>
@stop