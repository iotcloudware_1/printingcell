@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Edit Board</h3>
            </div>
            <form method="POST" action="{{ route('boards.update', $board->id) }}" enctype="multipart/form-data">
               {{csrf_field()}}
               {{method_field('PUT')}}
               <div class="row">
                <input type="hidden" name="board_gramage" class="form-control" value="{{$gramage->id}}">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Vendor Name</label>
                        <select name="vendor_id" class="form-control" required>
                        <option disabled="disabled" value="{{$board->vendor_id}}"  selected="selected">{{$board->vendor_name}}</option>
                          @foreach($vendor as $c)
                              <option value="{{$c['id']}}" >
                                  {{$c['name']}}
                              </option>
                          @endforeach
                      </select>
                     </div>
                  </div>
                  <!-- ad_link  --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Board Name</label>
                        <!-- <input name="name" value="{{$board->name}}" placeholder="company name" class="form-control" type="text" required> -->
                        <select id="test" name="name" class="form-control" required>
                        <option  disabled="disabled" selected="selected">{{$board->name}}</option>
                          @foreach($boards as $c)
                              <option  value="{{$c['name']}}" >
                                  {{$c['name']}}
                              </option>
                              
                          @endforeach
                          <option  class="editable">Others</option>
                          
                         
                      </select>
                       <input class="form-control editOption" style="display:none;"></input>
                     </div>
                  </div>
                 
               </div>
               <div class="row">
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Gramage</label>
                        <input name="gramage" value="{{$gramage->gramage}}"  placeholder="gramage" class="form-control" type="number" step="any" min="0" max="10000" required>
                     </div>
                     
                  </div>   
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Per Kg Cost</label>
                        <input name="per_kg_cost" value="{{$gramage->per_kg_cost}}" placeholder="per kg cost" class="form-control" type="number" step="any" min="0" max="10000" required>
                     </div>
                     
                  </div> 

                 
               </div>             
                <input type="hidden" name="board_id" value="{{$board->id}}">
               <button class="btn btn-success">Update</button>
            </form>
         </div>
         <!-- Form -->
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
     #billdesc{
    padding-top: 0px;
}
#test{
    width: 100%;
    height: 35px;
}
option {
    height: 30px;
    line-height: 30px;
}

.editOption{
    width: 30%;
    height: 26px;
    position: relative;
    top: -29px;
    /*background: #E0F5FF;;*/
    border: 0;
    /*padding-left: 14px;*/
    margin-left: 10px;
} 
    </style>
@stop

@section('js')
<script type="text/javascript">
  var initialText = $('.editable').val();
$('.editOption').val(initialText);

$('#test').change(function(){
var selected = $('option:selected', this).attr('class');
var optionText = $('.editable').text();

if(selected == "editable"){
  $('.editOption').show();

  
  $('.editOption').keyup(function(){
      var editText = $('.editOption').val();
      $('.editable').val(editText);
      $('.editable').html(editText);
  });

}else{
  $('.editOption').hide();
}
});
</script>

@stop