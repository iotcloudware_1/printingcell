@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Create Board</h3>
            </div>
            <form method="post" action="{{ route('boards.store') }}" enctype="multipart/form-data">
               @csrf
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Vendor Name</label>
                        <select name="vendor_id" class="form-control" required>
                        <option disabled="disabled" selected="selected">Select Vendor</option>
                          @foreach($vendor as $c)
                              <option value="{{$c['id']}}" >
                                  {{$c['name']}}
                              </option>
                          @endforeach
                      </select>
                     </div>
                  </div>
                  <!-- ad_link  --verified -->
                  
                  <div  class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div id="billdesc" class="form-group">
                        <label> Board Name</label>
                        <input name="name" placeholder="board name" class="form-control" type="text" required>
                     </div>
                  </div>
                 
               </div>
               <button class="btn btn-success">Create</button>
            </form>
         </div>
         <!-- Form -->
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
     #billdesc{
    padding-top: 0px;
}
#test{
    width: 100%;
    height: 35px;
}
option {
    height: 30px;
    line-height: 30px;
}

.editOption{
    width: 90%;
    height: 26px;
    position: relative;
    top: -29px;
    /*background: #E0F5FF;;*/
    border: 0;
    /*padding-left: 14px;*/
    margin-left: 10px;
} 
    </style>
@stop

@section('js')
<script type="text/javascript">
  var initialText = $('.editable').val();
$('.editOption').val(initialText);

$('#test').change(function(){
var selected = $('option:selected', this).attr('class');
var optionText = $('.editable').text();

if(selected == "editable"){
  $('.editOption').show();

  
  $('.editOption').keyup(function(){
      var editText = $('.editOption').val();
      $('.editable').val(editText);
      $('.editable').html(editText);
  });

}else{
  $('.editOption').hide();
}
});
</script>

@stop