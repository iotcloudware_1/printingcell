@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
@stop

@section('content')

 <div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
            <div class="heading-panel">
               <h3 class="main-title text-left">Add Gramage</h3>
            </div>
            <form method="post" action="{{ route('grammage.store') }}">
               @csrf
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Board</label>
                        <select name="board_id" class="form-control" required>
                        <option selected="selected">Select Board</option>
                          @foreach($board as $b)
                              <option value="{{$b['id']}}" >
                                  {{$b['name']}}
                              </option>
                          @endforeach
                      </select>
                     </div>
                  </div>
                  <!-- ad_link  --verified -->
                  
                  <div  class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Gramage</label>
                        <input name="gramage" placeholder="board name" class="form-control" type="text" required>
                     </div>
                  </div>

                  <div  class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                     <div class="form-group">
                        <label>Per kg cost</label>
                        <input name="per_kg_cost" placeholder="Per kg cost" class="form-control" type="text" required>
                     </div>
                  </div>
                 
               </div>
               <button class="btn btn-success">Create</button>
            </form>
         </div>
         <!-- Form -->
      </div>
@stop

@section('css')
 
@stop

@section('js')

@stop