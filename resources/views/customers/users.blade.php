@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1 style="text-align: center;">Add Costing</h1>
@stop

@section('content')
  

<div class="container">
<div class="col-md-12  col-sm-6 col-xs-12">
         <!--  Form -->
         <div class="form-grid">
           <!--  <div class="heading-panel">
               <h3 class="main-title text-left">Create Product</h3>
            </div> -->
            <form id="my-form" method="post" action="{{route('store-costing')}}" enctype="multipart/form-data">
               @csrf
               <div class="row">
                  <!-- category_type --verified -->
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Company Name</label>
                        <select class="customer-select form-control" required name="customer_id">
                        <option disabled="disabled" selected="selected">Select Company </option>
                           @foreach(Helper::customers() as $c)
                           <option value="{{$c['id']}}" >
                              {{$c['company_name']}}
                           </option>
                           @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Product Name</label>
                        <div id="product_parent">
                            <select class="product-select form-control" name="product_id">
                              <option disabled="disabled" selected="selected">Select Product </option>
                              @foreach(Helper::products() as $p)
                                  <option value="{{$p['id']}}" >
                                      {{$p['name_ar']}}
                                  </option>
                              @endforeach
                            </select>
                            
                          </div> 
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Type</label>
                        <select id="type" name="type" class="form-control" required>
                        <option disabled="disabled" selected="selected">Select Type</option>
                          <option value="0">Costing</option>
                          <option value="1">Proofing</option>
                          
                      </select>
                     </div>
                  </div>
                  <div class="col-md-3 col-lg-3 col-xs-6 col-sm-6">
                     <div class="form-group">
                        <label>Quality </label>
                        <select id="quality" name="quality" class="form-control" required>
                          <option disabled="disabled" selected="selected">Select quality</option>
                          <option value="0">Local</option>
                          <option value="1">Export</option>
                          
                      </select>
                     </div>
                  </div>
                  
                 
               </div>             

               <button class="btn btn-success"onclick="return clicked();">Add Costing</button>
            </form>
         </div>
         <!-- Form -->
  </div>
    <h3>Apply Custom Filters</h3>
    <div class="row">
      <div class="col-sm-4">
        <input type="text" name="customer_id" class="form-control searchCompany" placeholder="Search for Company Only...">    
      </div>
      <div class="col-sm-4">
        <input type="text" name="product_id" class="form-control searchEmail" placeholder="Search for Product Only...">    
      </div>      
      <div class="col-sm-4">
        <input type="text" name="proofing" class="form-control searchName" placeholder="Search for Type Only in Upper Case...">    
      </div>
    </div>
    

 <!--    <select name="email" class="form-control searchEmail" placeholder="Search for Email Only...">
    <option value="" selected="selected" disabled="disabled">Select Bla</option>   
    <option value="usamairshad321@gmail.com">usamairshad321@gmail.com</option>
    <option value="asd@asd.com">asd@asd.com</option>
    <option value="super_admin@costing.com">super_admin@costing.com</option>
    </select> -->
    <br>
    <table class="table table-bordered data-table">

        <thead>

            <tr>

                <th>No</th>

                <th>Product</th>
                <th>Company</th>


                <th>Type</th>
                <!-- <th>Board Type</th> -->
                 <th>Quality</th>
                <th>image</th>

                <th width="100px">Action</th>
                <th width="100px">Edit</th>
                <th width="100px">History</th>

            </tr>

        </thead>

        <tbody>

        </tbody>

    </table>

</div>
@stop

@section('css')
<style type="text/css">
.table th {
  text-align: center;
}
.table tr {
  text-align: center;
}  
/*.col, .col-1, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-10, .col-11, .col-12, .col-auto, .col-lg, .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-auto, .col-md, .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12, .col-md-auto, .col-sm, .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-auto
{
  background-image: linear-gradient(to top, #cfd9df 0%, #e2ebf0 100%);
}*/
</style>
@stop

@section('js')
<script src="{{ asset('js/customer-based-product.js') }}"></script>
<script type="text/javascript">
function clicked() {
      let price       = $('#type').val();
      let product       = $('#product_id').val();
      let quality       = $('#quality').val();


      if(price == 0)
      {
        price='Costing';
      }
      else if(price == 1)
      {
        price='Proofing';
      }
      if(quality == 0)
      {
        quality='Local';
      }
      else if(quality == 1)
      {
        quality='Export';
      }


    return confirm('Are you sure you want to add '+ price+' of '+ quality+ ' quality?');
}    
</script>

<script type="text/javascript">

  $(function () {

   

    var table = $('.data-table').DataTable({

        processing: true,

        serverSide: true,

        scrollX: false,


        ajax: {

          url: "{{ route('users.index') }}",

          data: function (d) {

                d.product_id = $('.searchEmail').val(),
                d.proofing = $('.searchName').val(),
                d.customer_id = $('.searchCompany').val(),

                d.search = $('input[type="search"]').val()

            }

        },

        columns: [

            {data: 'DT_RowIndex', name: 'DT_RowIndex'},

            {data: 'product_id', name: 'product_id'},

            {data: 'customer_id', name: 'customer_id'},

            {data: 'proofing', name: 'proofing'},

            // {data: 'board', name: 'board'},

            {data: 'is_local', name: 'is_local'},

            {data: 'image', name: 'image'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

            {data: 'edit', name: 'edit', orderable: false, searchable: false},

            {data: 'history', name: 'history', orderable: false, searchable: false},
        ]

    });

   

    $(".searchEmail").keyup(function(){

        table.draw();

    });

    $(".searchName").keyup(function(){

        table.draw();

    }); 
    $(".searchCompany").keyup(function(){

        table.draw();

    });    

  

  });

</script>
@stop