$(function() {
    $(".customer-select").on('change', function () {
        let districts = $('.product-select');
        $(districts).prop('disabled',true);
        let id = parseInt( $(this).val() );

        if(id>0){
            let url = "/customer/"+id+"/get-product"
            axios.get(url).then(function (response) {

                $(districts).html(response.data)
                $(districts).prop('disabled',false);


                $("#product_parent").html(response.data);
                             

            })

        }
    })
});
