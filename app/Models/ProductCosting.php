<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCosting extends Model
{
    protected $fillable = [
		
		'product_id',
		'vendor_id',
		'board_id',
		'is_proofing',
		'is_regular_board',
		'is_local',
		'is_draft',
		'board_length',
		'board_width',
		'board_gramage',
		'board_price',
		'board_box_transport',
		'no_of_board_packets',
		'no_of_printed_boxes',
		'board_cost_per_box',

		'no_of_colors',
		'printing_cost',
		'no_of_ups',
		'no_ups_for_printing',
		'total_printing_cost_per_box',
		
		'lamination_width',
		'lamination_length',
		'lamination_cost_per_square_inch',
		'lamination_cost_per_box',

		'no_of_boxed_in_carton',
		'carton_price',
		'packing_rate_per_box',

		'pasting_labor_cost_per_box',

		'colors_plate_cost',
		'per_plate_cost',
		'plate_cost',
		'dye_cost',
		'plate_and_dye_rate_per_box',
		'profit_percentage_of_dye_plates',
		
		'corrugation_cutting_size',
		'corrugation_width',
		'corrugation_per_square_inch_cost',
		'corrugation_transport',
		'corrugation_rate_per_box',
		'glue_rate',
		'corrugation_glue_rate_per_box',
		'total_cost_per_box',
		'total_price_per_box_comments',
		'profit_percentage_of_total_price',
		'created_by',
		'costing_date_and_time',
		'profit_percentage_calculation',
		'discounted_price',
	];

    public $table = "product_costing";
}
