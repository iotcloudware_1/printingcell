<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
   protected $fillable = [
		'name',
		'company_name',
		'mobile',
		'email',
		'description',
	];

    public $table = "customers";
}
