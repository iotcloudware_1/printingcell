<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grammage extends Model
{
    public $table = 'boardgramage';
    protected $fillable = ['board_id','gramage','per_kg_cost'];
}
