<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BoardGramage;

class Boards extends Model
{
    protected $fillable = [
		'name',
		'vendor_id',
	];

    public $table = "boards";

    public function gramage() {
        return $this->hasMany('App\Models\BoardGramage');
    }
    
}
