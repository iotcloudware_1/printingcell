<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
     protected $fillable = [
		'name',
		'mobile',
		'email',
		'description',
	];

    public $table = "vendors";
}
