<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoardGramage extends Model
{
     protected $fillable = [
		'board_id',
		'gramage',
		'per_kg_cost',
	];

    public $table = "boardgramage";

    public function board() {
        return $this->belongsTo('App\Models\Boards');
    }
}
