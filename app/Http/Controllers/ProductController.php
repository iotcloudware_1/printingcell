<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\StoreAdRequest;
use App\Models\Customers;
use App\Models\Products;
use App\Models\ProductImage;
use App\Models\Order;
use App\Models\ProductCosting;
use App\Models\ProductCostingUpdate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use App\DataTables\ProductsDataTable;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        // dd("asdas");
        // // $tailor= ProductCosting::all();
        // $temp= DB::table('product_costing')
        //         ->join('product_costing_update','product_costing.id','product_costing_update.product_costing_id')
                
        //         ->select('product_costing_update.*','product_costing.product_id')
        //         ->orderBy('created_at','DESC')
        //         // ->distinct()
        //         ->latest()
        //         ->get();
        // $temp= DB::table('product_costing')
        //     ->join('product_costing_update', 'product_costing.id', '=', 'product_costing_update.product_costing_id')
        //     ->select('product_costing_update.*','product_costing.product_id')
        //     ->distinct('product_costing_update.product_costing_id')
        //     ->count();
         
           // $temp = DB::table('product_costing')
           //  ->join('product_costing_update', 'product_costing.id', '=', 'product_costing_update.product_costing_id')            
           //  ->select('product_costing_update.*','product_costing.product_id','product_costing.id as costing')
           //  ->groupBy('product_costing_id')
           //  ->get();

            // $temp= DB::table('product_costing_update')
            //       ->leftJoin('product_costing', 'product_costing_update.product_costing_id', '=', 'product_costing.id')
            //       ->orderBy('product_costing_update.created_at','DESC')
            //       ->get();

            // return $temp->groupBy('product_costing_id');

            $tailor = ProductCostingUpdate::whereRaw('Created_at IN (select MAX(Created_at) FROM product_costing_update GROUP BY PRODUCT_COSTING_ID)')->get();        
            
            // dd($data);   

        // $tailor=(array)$temp;
        
        $product= Products::select('name','id')->get();
        $filtered="no-filter";

        // return view('welcome',compact('tailor','product','filtered'));

        return view('dashboard',compact('tailor','product','filtered'));
    }


    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer= Customers::all();
        return view('products.create',compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $input = $request->except('image');
        $images = $request->file('image');
        $product = Products::create($input); 
        if($request->hasFile('image'))
        {
            foreach ($images as $image) {
                $input = $request->all();
                $imageName = time().'.'.$image->extension(); 
                $image->move('uploads/product-images', $imageName);
                $path = 'uploads/product-images/'.$imageName;
                $product_images = ProductImage::Create([
                    'product_id' => $product->id,
                    'path' => $path,
                ]);
            }
        }
        Alert::success('Product Created successfully');
        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $product=Products::where('id',$id)->first();
         $product->customer_id= Helper::customerIdToName($product->customer_id);
         $customer= Customers::all();
         return view('products.edit',compact('customer','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        Products::where('id',$request['product_id'])->update(['name'=>$request['name'],'customer_id'=>$request['customer_id'],'description'=>$request['description']]);   
        
         if($request->hasFile('image'))
        {

            $input = $request->all();

            $image = $request->file('image');
            $imageName = time().'.'.$image->extension(); 
            $image->move('uploads/product-images', $imageName);
            
            Products::where('id',$input['product_id'])->update(['image'=>'uploads/product-images/'.$imageName]);   


        }
      
         Alert::success('Product updated successfully');
        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function remove($id)
    {
        Products::where('id',$id)->delete();
        Alert::success('Product deleted successfully');
        return redirect('products');

    }
    public function getProduct($id)
    {
        $get_products = Products::where('customer_id',$id)->get();

        if(count($get_products) > 0){
            ?>
            <input name="dummy" class="form-control" type="hidden" value="found" />
            <select class="product-select form-control"  name="product_id">

               <option disabled="disabled" selected="selected">Product </option>>

              <?php 
              foreach($get_products as $product){ 

                echo '<option value="'.$product['id'].'" > '.$product['name'].'</option>';

                
                } 
                ?>
                  
            </select>
    
            <?php
        }else{
            echo '<input value="No Product Found" readonly name="product_id" type="text" /> 
                  <input name="dummy" class="form-control" type="hidden" value="not_found" />';
        }
    }
}
