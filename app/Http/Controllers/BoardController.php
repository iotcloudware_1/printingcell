<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\StoreAdRequest;
use App\Models\Vendors;
use App\Models\Boards;
use App\Models\BoardGramage;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use App\DataTables\BoardsDataTable;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BoardsDataTable $dataTable)
    {
        return $dataTable->render('grammages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $board= Boards::all();
        return view('grammages.create',compact('board'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $grammage = Gramage::create($input);
        Alert::success('Board Created successfully');
        return redirect('boards');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $gramage=BoardGramage::where('id',$id)->first();
        $board=Boards::where('id',$gramage->board_id)->first();
        // dd($gramage);
        $board->vendor_name= Helper::vendorIdToName($board->vendor_id);
        $vendor= Vendors::all();
        $boards= Boards::all();
        return view('boards.edit',compact('vendor','board','boards','gramage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);   
        Boards::where('id',$request->board_id)->update(['name'=>$request->name,'vendor_id'=>$request->vendor_id]);
        BoardGramage::where('id',$request->board_gramage)->update(['gramage'=>$request->gramage,'per_kg_cost'=>$request->per_kg_cost]);
         Alert::success('Board updated successfully');
        return redirect('boards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getBoards(Request $request)
    {
        $boards=Boards::where('vendor_id',$request['id'])->get();
        return $boards;
    }
    public function getBoardDetails(Request $request)
    {
        $boards=Boards::where('id',$request['id'])->first();
        return $boards;   
    }

    public function getBoardGrammage(Request $request)
    {
        $boards=BoardGramage::where('board_id',$request['id'])->get();
        return $boards;   
    }
    
    public function getGrammagePrice(Request $request)
    {
        $boards=BoardGramage::where('id',$request['id'])->get();
        return $boards;
    }
    
    public function getIdGramage(Request $request)
    {
        $boards=BoardGramage::where('id',$request['id'])->value('gramage');
        return $boards;
    }
    public function remove($id)
    {
        BoardGramage::where('id',$id)->delete();
        Alert::success('Board deleted successfully');
        return redirect('boards');
    }
}
