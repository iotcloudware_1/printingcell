<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\StoreAdRequest;
use App\Models\Customers;
use App\Models\Products;
use App\Models\ProductImage;
use App\Models\ProductCosting;
use App\Models\ProductCostingUpdate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use App\DataTables\ProductCostingDataTable;
use App\DataTables\ProductCostingUpdatedDataTable;

class ProductCostingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(ProductCostingDataTable $dataTable)
    {
        return $dataTable->render('products.costing');
    }
    public function updatedCosting(ProductCostingUpdatedDataTable $dataTable)
    {
        return $dataTable->render('products.update-costing');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeCosting(Request $request)
    {
        
        $input=$request->all();
        $input['created_by']= Auth::id();
        // dd($input);
        return view('products.add-costing',compact('input'));
    }

    public function getDetails($id)
    {
        $product = ProductCosting::find($id);
        $temp=ProductCostingUpdate::where('product_costing_id',$id)->orderBy('created_at','DESC')->first();
        $data=Products::where('id',$product->product_id)->first();
        $company= Customers::where('id',$data->customer_id)->value('company_name');
        // return view('products.product-data', compact('product','data'));
        $images = ProductImage::where('product_id',$product->product_id)->pluck('path');
        return view('products.test-details', compact('product','data','company','temp','images'));
    }
    public function editCosting($id)
    {
        if(ProductCostingUpdate::where('product_costing_id',$id)->orderBy('created_at','DESC')->first())
        {
            $input = ProductCostingUpdate::where('product_costing_id',$id)->orderBy('created_at','DESC')->first();
            $temp='found';
            // $data=Products::where('id',$product->product_id)->first();
            // $company= Customers::where('id',$data->customer_id)->value('company_name');

            return view('products.edit-costing',compact('input','temp'));
        }
        else{
            
            $input = ProductCosting::find($id);
            $temp='not_found';

            // $data=Products::where('id',$product->product_id)->first();
            // $company= Customers::where('id',$data->customer_id)->value('company_name');

            return view('products.edit-costing',compact('input','temp'));
        }   

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function submitCosting(Request $request)
    {
        \Log::info($request);
        $input=$request->all();
        $input['created_by']=Auth::id();

        ProductCosting::create($input);
        $data= ProductCosting::orderBy('created_at','DESC')->first();        

        ProductCostingUpdate::create($input);


        $temp=ProductCostingUpdate::orderBy('created_at','DESC')->first();
        $temp->product_costing_id=$data->id;
        $temp->save();


    }

    public function updateCosting(Request $request)
    {

        $input=$request->all();
        // $input['created_by']=Auth::id();
        ProductCostingUpdate::create($input); 
    }

    public function getUpdatedDetails($id)
    {
        $product = ProductCostingUpdate::find($id);
        $data=Products::where('id',$product->product_id)->first();
        $company= Customers::where('id',$data->customer_id)->value('company_name');
        // return view('products.product-data', compact('product','data'));

        return view('products.test-details', compact('product','data','company'));   
    }

    public function filterizedProduct(Request $request)
    {
        
        // dd($request);
        $filtered="filtered";
        // $named=[];
        // foreach ($request as $key => $value) {
        //     $name=Helper::productIdToName($request['states']);
        //     $type=Helper::proofingIdToName($request['type']);

        //     array_push($named, $name);
        // }
        $new=$request['states'];
        $type=$request['type'];
        if ($request['type'] == 1  &&  $request['states']) 
        {
            // dd("1");
            $tailor= ProductCosting::whereIn('product_id',$request['states'])->where('is_proofing',1)->get();
            $product= Products::select('name','id')->get();
            // $filter="filtered";

           
        }
        elseif ($request['type'] == 2  &&  $request['states']) 
        {
            // dd("2");
            $tailor= ProductCosting::whereIn('product_id',$request['states'])->where('is_proofing',0)->get();
            $product= Products::select('name','id')->get();
            // $filter="filtered";

            // return view('welcome',compact('tailor','product'));
        }
        elseif($request['states'])
        {   
            // dd("3");
            $tailor= ProductCosting::whereIn('product_id',$request['states'])->get();
            $product= Products::select('name','id')->get();
            // $filter="filtered";

            // return view('welcome',compact('tailor','product'));            
        }
        elseif ($request['type'] == 1  || $request['type'] == 2 ) 
        {
            // dd("4");
            if($request['type'] == 1)
            {
                $tailor= ProductCosting::where('is_proofing',1)->get();
                $product= Products::select('name','id')->get();
                // $filter="filtered";

                // return view('welcome',compact('tailor','product'));                
            }
            elseif ($request['type'] == 2) 
            {
                $tailor= ProductCosting::where('is_proofing',0)->get();
                $product= Products::select('name','id')->get();
                // $filter="filtered";

                // return view('welcome',compact('tailor','product'));
            }

        }

        // elseif ($request['type'] && $request['states'] ||  $request['type']== 0) 
        // {

        //     $tailor= ProductCosting::whereIn('product_id',$request['states'])->where('is_proofing',$request['type'])->get();
        //     $product= Products::select('name','id')->get();
        //     // $filter="filtered";

        //     return view('welcome',compact('tailor','product'));
        // }
        else
        {
            $tailor= ProductCosting::all();
            $product= Products::select('name','id')->get();

            // return view('welcome',compact('tailor','product'));

        }

        return view('welcome',compact('tailor','type','product','new','filtered'));

    }
}
