<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\StoreAdRequest;
use App\Models\Vendors;
use App\Models\Product;
use App\User;
use Carbon\Carbon;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use App\DataTables\VendorsDataTable;
use Auth;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(VendorsDataTable $dataTable)
    {
        return $dataTable->render('vendors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Vendors::create($input);
        Alert::success('Vendor Created successfully');
        return redirect('vendors');
    }

    public function getAuthUser()
    {
        $vendor=User::where('id',Auth::id())->first();

        return $vendor;

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $vendor=Vendors::where('id',$id)->first();

        return view('vendors.edit',compact('vendor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Vendors::where('id',$request->vendor_id)->update(['name'=>$request->name,'email'=>$request->email,'mobile'=>$request->mobile,'description'=>$request->description]);
         Alert::success('Vendor updated successfully');
        return redirect('vendors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function remove($id)
    {
        Vendors::where('id',$id)->delete();
        Alert::success('Vendor deleted successfully');
        return redirect('vendors');

    }
     public function statusUpdated(Request $request)
    {

        $user=Vendors::where('id',$request->id)->update(['status'=>$request->status]);
       
        // Alert::success('Customer'.$request->status.'successfully');
        return $user;
    }

    public function getVendors()
    {
        $vendor=Vendors::all();

        return $vendor;
    }
    // public function remove($id)
    // {
    //     Vendors::where('id',$id)->delete();
    //     Alert::success('Vendor deleted successfully');
    //     return redirect('vendors');

    // }
}
