<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Http\Controllers\Controller as Controller;
use App\Http\Requests\StoreAdRequest;
use App\Models\Customers;
use App\Models\Products;
use App\Models\Order;
use App\Models\BoardGramage;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use RealRashid\SweetAlert\Facades\Alert;
use App\DataTables\CustomersDataTable;
use App\Models\ProductCosting;

use DataTables;

use Illuminate\Support\Str; 

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomersDataTable $dataTable)
    {
        return $dataTable->render('customers.index');
    }

    // public function dummyIndex(Request $request)
    // {
    //     if ($request->ajax()) {

              

    //         $data = ProductCosting::latest()->get();

   

    //         return Datatables::of($data)

    //                 ->addIndexColumn()

    //                 ->filter(function ($instance) use ($request) {

    //                     if (!empty($request->get('email'))) {

    //                         $instance->collection = $instance->collection->filter(function ($row) use ($request) {

    //                             return Str::contains($row['email'], $request->get('email')) ? true : false;

    //                         });

    //                     }
    //                     if (!empty($request->get('name'))) {

    //                         $instance->collection = $instance->collection->filter(function ($row) use ($request) {

    //                             return Str::contains($row['name'], $request->get('name')) ? true : false;

    //                         });

    //                     }

   

    //                     if (!empty($request->get('search'))) {

    //                         $instance->collection = $instance->collection->filter(function ($row) use ($request) {

    //                             if (Str::contains(Str::lower($row['email']), Str::lower($request->get('search')))){

    //                                 return true;

    //                             }else if (Str::contains(Str::lower($row['name']), Str::lower($request->get('search')))) {

    //                                 return true;

    //                             }

   

    //                             return false;

    //                         });

    //                     }

   

    //                 })

    //                 ->addColumn('action', function($row){

  

    //                        $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';

  

    //                         return $btn;

    //                 })

    //                 ->rawColumns(['action'])

    //                 ->make(true);

    //     }

    

    //     return view('customers.users');        
    // }

     public function dummyIndex(Request $request)
    {
        if ($request->ajax()) {

            $temp = DB::table("product_costing")
                ->join("products" , "product_costing.product_id", "=", "products.id")
                // ->where('ads.ad_status','=','pending')
                ->select('product_costing.*','products.customer_id','products.image as image','product_costing.id as temp','products.image as image','product_costing.is_proofing as proofing','product_costing.is_regular_board as board','product_costing.is_local as quality')
                ->orderBy('created_at','DESC')
                ->get();
             $data=[];   

            foreach ($temp as $key => $value) 
            {   
                $w_h = (array)$value;

                $w_h['product_id']=Helper::productIdToName($w_h['product_id']);
                $w_h['customer_id']=Helper::customerIdToCompany($w_h['customer_id']);
                $w_h['is_proofing']=Helper::proofingIdToName($w_h['is_proofing']);
                $w_h['is_regular_board']=Helper::boardToName($w_h['is_regular_board']);
                $w_h['is_local']=Helper::qualityIdToName($w_h['is_local']);
                array_push($data, $w_h);

            }

   

            return Datatables::of($data)

                    ->addIndexColumn()

                    ->filter(function ($instance) use ($request) {

                        if (!empty($request->get('product_id'))) {

                            $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                                return Str::contains($row['product_id'], $request->get('product_id')) ? true : false;

                            });

                        }
                        if (!empty($request->get('proofing'))) {

                            $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                                return Str::contains($row['proofing'], $request->get('proofing')) ? true : false;

                            });

                        }

                        if (!empty($request->get('customer_id'))) {

                            $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                                return Str::contains($row['customer_id'], $request->get('customer_id')) ? true : false;

                            });

                        }

   

                        if (!empty($request->get('search'))) {

                            $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                                if (Str::contains(Str::lower($row['product_id']), Str::lower($request->get('search')))){

                                    return true;

                                }else if (Str::contains(Str::lower($row['proofing']), Str::lower($request->get('search')))) {

                                    return true;

                                }else if (Str::contains(Str::lower($row['customer_id']), Str::lower($request->get('search')))) {

                                    return true;

                                }
                                
   

                                return false;

                            });

                        }

   

                    })

                    ->addColumn('action', function($row){

  

                           $btn ='<a href="/product/'.$row['temp'].'/details"><button class="btn btn-primary"value="'.$row['temp'].'">View</button></a>';



  

                            return $btn;

                    })
                    ->addColumn('edit', function($row){

  

                           $btn ='<a href="/product/'.$row['temp'].'/edit"><button class="btn btn-primary"value="'.$row['temp'].'">Edit</button></a>';
                            


  

                            return $btn;

                    })

                    ->addColumn('history', function($row){

                           $btn ='<a href="/product-costing/'.$row['temp'].'/history"><button class="btn btn-primary">History</button></a>';


                            return $btn;

                    })

                    ->addColumn('image', function ($row) { 
                          $url=asset($row['image']); 
                           return '<img src='.$url.' border="0" height="50" class="img-rounded img-responsive" align="center" />'; 
                    })
                    ->editColumn('proofing',function ($row){

                            switch(Helper::proofingIdToName($row['proofing'])){
                                case "Costing":
                                    $class = "bg-success";
                                    break;
                                case "Proofing":
                                    $class = "bg-info";
                                    break;
                                case "None":
                                    $class = "bg-primary";
                                    break;    
                            }

                        return '<span class="badge '.$class.'">'.strtoupper(Helper::proofingIdToName($row['proofing'])).'</span>';
                        // return $return;
                    }) 
                    // ->editColumn('board',function ($row){

                    //         switch(Helper::boardToName($row['board'])){
                    //             case "Irregular":
                    //                 $class = "bg-success";
                    //                 break;
                    //             case "Regular":
                    //                 $class = "bg-info";
                    //                 break;
                    //             case "None":
                    //                 $class = "bg-primary";
                    //                 break;    
                    //         }

                    //     return '<span class="badge '.$class.'">'.strtoupper(Helper::boardToName($row['board'])).'</span>';
                    //     // return $return;
                    // }) 
                     ->editColumn('is_local',function ($row){

                            switch(Helper::qualityIdToName($row['quality'])){
                                case "Local":
                                    $class = "bg-success";
                                    break;
                                case "Export":
                                    $class = "bg-info";
                                    break;  
                            }

                        return '<span class="badge '.$class.'">'.strtoupper(Helper::qualityIdToName($row['quality'])).'</span>';
                        // return $return;
                    })
                    ->rawColumns(['image', 'action'])

                
                    ->escapeColumns([])

                    ->make(true);


        }

    

        return view('customers.users');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        Customers::create($input);
        Alert::success('Customer Created successfully');
        return redirect('customers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer=Customers::where('id',$id)->first();

        return view('customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Customers::where('id',$request->customer_id)->update(['name'=>$request->name,'company_name'=>$request->company_name,'email'=>$request->email,'mobile'=>$request->mobile,'description'=>$request->description]);
        Alert::success('Customer updated successfully');
        return redirect('customers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function remove($id)
    {
        Customers::where('id',$id)->delete();
        Alert::success('Customer deleted successfully');
        return redirect('customers');

    }
    public function statusUpdated(Request $request)
    {

        $user=Customers::where('id',$request->id)->update(['status'=>$request->status]);
       
        // Alert::success('Customer'.$request->status.'successfully');
        return $user;
    }
    public function test(Request $request)
    {

        $new=BoardGramage::select('gramage')
                 ->groupBy('gramage')
                 ->having('gramage','>','250')
                 ->get();

                            // ->groupBy('gramage');
        
        dd($new);
        // Alert::success('Customer'.$request->status.'successfully');
        // return $user;
    }
}
