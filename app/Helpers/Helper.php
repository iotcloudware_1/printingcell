<?php

namespace App\Helpers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use App\User;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Vendors;
use App\Models\Boards;
use App\Models\ProductImage;
class Helper
{

    public static function products()
    {
        $product=Products::all();
        return $product;
    }
    public static function customers()
    {
        $customer= Customers::all();
        return $customer;
    }

    public static function customerIdToName($id)
    {
        $name = Customers::where('id',$id)->value('name');
        return $name;
    }

    public static function customerIdToCompany($id)
    {
        $name = Customers::where('id',$id)->value('company_name');
        return $name;
    }
    public static function vendorIdToName($id)
    {
        $name = Vendors::where('id',$id)->value('name');
        return $name;
    }
    public static function userIdToName($id)
    {
        $name = User::where('id',$id)->value('name');
        return $name;
    }
    
    public static function qualityIdToName($id)
    {
       if($id == 0)
        {
            return "Local";
        }
        else
        {
            return "Export";
        }
    }

	public static function categoryIdToName($id)
    {
        $name = Category::where('id',$id)->value('name');
        return $name;
    }
    public static function productIdToName($id)
    {
        $name = Products::where('id',$id)->value('name');
        return $name;
    }
    public static function productIdToImage($id)
    {
        $name = Products::where('id',$id)->value('image');
        return $name;
    }
    public static function proofingIdToName($id)
    {
        if($id == 0)
        {
            return "Costing";
        }
        else
        {
            return "Proofing";
        }

    }
    public static function getProductImage($id)
    {
        $image = ProductImage::where('product_id',$id)->value('path');
        return $image;
    }
    public static function boardToName1($id)
    {
        $board_name = Boards::where('id',$id)->value('name');
        return $board_name;
    }

    public static function boardToName($id)
    {
        if (isset($id)) 
        {
           if($id == 0)
            {
                return "Irregular";
            }
            elseif($id == 1)
            {
                return "Regular";
            }
            // elseif(isset($id))
            // {
            //     return "None";
            // }
            // else
            // {

            // }
        }    
        else
        {
            return "None";
        }


    }


}

