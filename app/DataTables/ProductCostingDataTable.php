<?php

namespace App\DataTables;

use App\Models\ProductCosting;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Auth;
use App\Helpers\Helper; 
class ProductCostingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('image', function ($query) { 
                   $url=asset("$query->image"); 
                   return '<img src='.$url.' border="0" height="50" class="img-rounded img-responsive" align="center" />'; 
            })
            ->addColumn('company', function ($query){

                return Helper::customerIdToCompany($query->customer_id);   
            })  
            ->addColumn('product', function ($query){

                return Helper::productIdToName($query->product_id);   
            })
            ->addColumn('created_by', function ($query){

                return Helper::userIdToName($query->creted_by);   
            })
            ->editColumn('details', function ($query){
                // return '<a href="#" onclick="loadProductData(this)"  data-user_id="'.$query->id.'" ><button class="btn btn-primary"value="$query->id">View</button></a>';
                  return '<a href="/product/'.$query->id.'/details"><button class="btn btn-primary"value="$query->id">View</button></a>';
            })
            ->editColumn('type',function ($query){

                switch(Helper::proofingIdToName($query->is_proofing)){
                    case "Costing":
                        $class = "bg-success";
                        break;
                    case "Proofing":
                        $class = "bg-info";
                        break;
                }

                return '<span class="badge '.$class.'">'.strtoupper(Helper::proofingIdToName($query->is_proofing)).'</span>';
            }) 
            ->editColumn('quality',function ($query){

                switch(Helper::qualityIdToName($query->is_local)){
                    case "Local":
                        $class = "bg-success";
                        break;
                    case "Export":
                        $class = "bg-info";
                        break;
                }

                return '<span class="badge '.$class.'">'.strtoupper(Helper::qualityIdToName($query->is_local)).'</span>';
            })            

            // ->addColumn('board', function ($query){

            //     return Helper::boardToName($query->is_regular_board);   
            // })
            ->editColumn('board',function ($query){

                switch(Helper::boardToName($query->is_regular_board)){
                    case "Irregular":
                        $class = "bg-success";
                        break;
                    case "Regular":
                        $class = "bg-info";
                        break;
                    case "None":
                        $class = "bg-primary";
                        break;    
                }

                return '<span class="badge '.$class.'">'.strtoupper(Helper::boardToName($query->is_regular_board)).'</span>';
            })             
            ->addColumn('action', 'products.product-action')
        
            ->escapeColumns([]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ProductCosting $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductCosting $model)
    {
        return $model->newQuery()
                ->select("product_costing")
                ->join("products" , "product_costing.product_id", "=", "products.id")
                // ->where('ads.ad_status','=','pending')
                ->select('product_costing.*','products.customer_id','products.image as image');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('productcosting-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    // ->dom('Bfrtip')
                    ->orderBy(1);
                    // ->buttons(
                    //     Button::make('create'),
                    //     Button::make('export'),
                    //     Button::make('print'),
                    //     Button::make('reset'),
                    //     Button::make('reload')
                    // );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')
            ->addClass('text-center'),
            Column::make('company')
            ->addClass('text-center'),            
            Column::make('product')
            ->addClass('text-center'),
            Column::make('created_by')
            ->addClass('text-center'),
            Column::make('image'),            
            Column::make('type')
            ->addClass('text-center'),
            Column::make('quality')
            ->addClass('text-center'),
            Column::make('board')
            ->addClass('text-center'),
            Column::make('board_cost_per_box')
            ->addClass('text-center'),
            Column::make('total_price_per_box_comments')
            ->addClass('text-center'),
            // Column::make('profit_percentage_of_total_price'),
            // Column::make('created_by'),
            // Column::make('created_at'),
            Column::make('details'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
                  
            // Column::make('delete'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ProductCosting_' . date('YmdHis');
    }
}
