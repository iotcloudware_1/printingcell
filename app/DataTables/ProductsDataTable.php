<?php

namespace App\DataTables;

use App\Models\Products;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Auth;
use App\Helpers\Helper; 

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addColumn('Edit', 'products.action')
            ->addColumn('image', function ($query) {
                $image = Helper::getProductImage($query->id);
                $url=asset($image); 
                return '<img src='.$url.' border="0" height="50" class="img-rounded img-responsive" align="center" />'; 
            })
            ->addColumn('customer_name', function ($query){

                return Helper::customerIdToName($query->customer_id);   
            })
            ->addColumn('company_name', function ($query){

                return Helper::customerIdToCompany($query->customer_id);   
            })
            ->addColumn('delete', function ($query){

            $return = '<a href="/product/'.$query->id.'/delete"><i class="ml-2 fas fa-trash" style="color: maroon"></i></a>';
            return $return;
            })
            ->escapeColumns([]);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Products $model)
    {
        return $model->newQuery()
                ->select("products")
                ->join("customers" , "products.customer_id", "=", "customers.id")
                ->where('customers.status','=','active')
                ->select('products.*');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('products-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create')->action("window.location = '".route('products.create')."';"),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('name'),
             Column::make('image'),

            Column::make('customer_name'),
            Column::make('company_name'),
            Column::make('description'),
            // Column::make('created_at'),
            Column::computed('Edit')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('delete'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Products_' . date('YmdHis');
    }
}
