<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

		$permissionsByRole = [

		    'super_admin' => [
			    ['name'=>'vendor_list'],
			    ['name'=>'vendor_create'],
			    ['name'=>'vendor_edit'],
			    ['name'=>'vendor_show'],
			    ['name'=>'vendor_delete'],

			    ['name'=>'product_list'],
			    ['name'=>'product_create'],
			    ['name'=>'product_edit'],
			    ['name'=>'product_show'],
			    ['name'=>'product_delete'],
 				
 				['name'=>'board_list'],
			    ['name'=>'board_edit'],
			    ['name'=>'board_show'],
			    ['name'=>'board_delete'],

			    ['name'=>'customer_list'],
			    ['name'=>'customer_edit'],
			    ['name'=>'customer_show'],
			    ['name'=>'customer_delete'],
		    ],
		    
		  //   'tailor' => [

			 //    ['name'=>'product_list'],
			 //    ['name'=>'product_show'],

				// ['name'=>'order_list'],
			 //    ['name'=>'order_show'],
			    

		  //   ],
		    
		  //   'customer' => [
			 //    ['name'=>'product_list'],
			 //    ['name'=>'product_show'],

				// ['name'=>'order_list'],
			 //    ['name'=>'order_show'],
		  //   ],
		    
		];

		foreach ($permissionsByRole as $role => $permissionIds) {
			$permissions = Permission::insert($permissionIds);
		    $_role = Role::create(['name' => $role ]);

		   	$_role->givePermissionTo($permissionIds);
		}
    }
}
